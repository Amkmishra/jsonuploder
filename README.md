This contains the files required to build the images as well as postman collection and environment details.

Steps:
1) import the collection and environment in postman - available in postman folder, a sample csv is also available
2)
	Run below commands to create image and bring up the application
	 docker build . --tag jsonuploder
	 docker-compose up -d
	 
	 app url: http://localhost:8080
	 Default user/password is admin/admin
	 
	 One entity named employee has been created and will be available in "Entities" dropdown after login.
	 